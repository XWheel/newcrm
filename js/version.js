/**
 * Created by vkwk on 8/14/15.
 */

var versionDownloadList;

function reloadVersionList()
{
    $.get("app/version/list",function(data,status){
        if(data.status===0){
            renderVersionList(data);
            versionDownloadList=data.data;
        }else
        {

        }

    });

}

function renderVersionList(data)
{
    var html=template("versionModule",data);
    var list=document.getElementById("VersionList");
    list.innerHTML=html;

}

function downloadApk(url)
{
    window.open(versionDownloadList[url].url);
}

function deleteApk(vid)
{
    $.post("app/version/del",{"id":vid},function(data,status){
       if(data.status===0){
           alert("版本删除成功");
           window.location.reload();
       } else
       {

       }
    });

}


function renderUrlList(data)
{
    console.log(data);
    var html=template("urlListModule",data);
    var list=document.getElementById("UrlList");
    list.innerHTML=html;
}

function reloadUrlList()
{
    $.get("url/get",function(data,status){

        if(data.status===0)
        {
            renderUrlList(data);

        }else
        {


        }

    });

}


function resetUrl(uid)
{
    var url=document.getElementById("reseturl"+uid).value;
    console.log(url);
    if(url.length===0){
        alert("请填写正确的url");
        return;
    }else
    {
        $.post("url/update",{"id":uid,"redirect":url},function(data,status){
           if(data.status===0)
           {
                alert(data.msg);
                window.location.reload();
           }else
           {
               alert("更新失败,错误原因:"+data.msg);

           }
        });
    }

}

function deleteUrl(uid)
{
    alert("删你妹啊删");

}

function addUrl()
{
    var name=document.getElementById("newUrlName").value;
    var originUrl=document.getElementById("originUrl").value;
    var redirectUrl=document.getElementById("redirectUrl").value;
    if(name.length===0){
        alert("没有名字");
        return;
    }
    if(originUrl.length===0) {
        alert("原始地址有误");
        return;
    }
    if(redirectUrl.length===0){
        alert("跳转地址有误");
        return;
    }
    $.post("url/add",{"name":name,"url":originUrl,"redirect":redirectUrl},function(data,status){
        if(data.status===0){
            alert("添加成功");
            window.location.reload();
        }else
        {
                alert(data.msg);
        }
    });
}