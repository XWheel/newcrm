/**
 * Created by vkwk on 7/10/15.
 */

    var isuploading = false;


    function upload(){

    uploadAndSubmit()

    }

    function uploadApk()
    {
        var version=document.getElementById("versionNumber").value;
        var filesList=document.getElementById("filelist");

        var form = new FormData();

        form.append("app",filesList.files[0]);
        form.append("version",version);

        console.log(form);

        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            console.log("Upload complete.");
        };
        xhr.onreadystatechange = function(){
            if(xhr.readyState ==4)
            {
                if(xhr.status == 200){
                    alert("上传成功");
                    isuploading=false;
                    window.location.reload();
                }else
                {
                    alert("上传失败");
                    isuploading=false;
                }
            }
        }
        xhr.open("post", "app/upload", true);
        isuploading=true;
        xhr.send(form);
    }

    function uploadAD(){

    }

    function AddedFile(){
        var filelist=document.getElementById("filelist");
        var label=document.getElementById("FileStatus");
        if(filelist.files.length>0){
            var file=filelist.files[0];
            label.innerHTML="已选择 "+ file.name;
        }else
        {
            label.innerHTML="选择上传的文件";
        }
    }

function AddedPic(){
    var filelist=document.getElementById("filelist");
    var label=document.getElementById("FileStatus");
    if(filelist.files.length>0){
        var file=filelist.files[0];
        label.innerHTML="已选择 "+ file.name;
        //var previewZone=document.getElementById("previewPic");
        //var url=getObjectURL(file);
        //previewZone.innerHTML="<img src="+url+"/>";
    }else
    {
        label.innerHTML="选择上传的文件";
    }
}


    function deselectFile(){
        var fileList=document.getElementById("filelist");
        if (fileList.length>0){
            var file=fileList[fileList.length-1];
            fileList.removeChild(file);
        }else
        {
            alert("并没有文件给你删除");
        }
    }


function uploadAndSubmit() {

    var filesList=document.getElementById("filelist");
    var form = new FormData();
    for(var i=0;i<filesList.length;i++)
    {
        form.append("file",filesList.files[i]);
    }
    console.log(form);
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        console.log("Upload complete.");
    };
    xhr.onreadystatechange = function(){
        if(xhr.readyState ==4)
        {
            if(xhr.status == 200){
                alert("成功");
                isuploading=false;
                window.location.reload();
            }else
            {
                alert("图片上传失败");
                isuploading=false;
            }

        }

    }

    xhr.open("post", "picture/upload", true);
    isuploading=true;
    xhr.send(form);
}


function getObjectURL(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}